﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{

    public Camera MainCamera;
    public GameObject Skydome;
    public Light Skylight;

    private static float MOVEMENT_SPEED = 20.0f;
    private static float ROTATION_HOR_SPEED = 50.0f;

    private float skydomeRadius;
    private float skydomeHeight;

	// Use this for initialization
	void Start ()
    {
        // Kamera auf Nullpunkt positionieren
        MainCamera.transform.position = new Vector3(0.0f, 1.7f, 0.0f); // Sicht aus 1.70m Höhe :)
        MainCamera.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);

        // Werte für Skydome merken
        Renderer renderer = Skydome.GetComponent<Renderer>();
        Vector3 size = renderer.bounds.size;

        this.skydomeRadius = size.x / 2.0f;
        this.skydomeHeight = size.y;
        
        positionSkydome();
    }
	
	// Update is called once per frame
	void Update ()
    {
        float mouseX = Input.GetAxis("Mouse X");        
        float movement = MOVEMENT_SPEED * Time.deltaTime;
        float rotation = ROTATION_HOR_SPEED * Time.deltaTime;

        Vector3 forward = MainCamera.transform.forward;
        Vector3 right = MainCamera.transform.right;        
        Vector3 up = MainCamera.transform.up;

        if (Input.GetKey(KeyCode.W))
        {
            MainCamera.transform.Translate(forward * movement);
        }

        if (Input.GetKey(KeyCode.S))
        {
            MainCamera.transform.Translate(-forward * movement);
        }

        if (Input.GetKey(KeyCode.A))
        {
            MainCamera.transform.Translate(-right * movement);
        }

        if (Input.GetKey(KeyCode.D))
        {
            MainCamera.transform.Translate(right * movement);
        }
        
        MainCamera.transform.Rotate(up * rotation * mouseX);
        positionSkydome();
	}

    /// <summary>
    ///  Positioniert den Skydome und das Skylight über der Kamera.
    /// </summary>
    void positionSkydome()
    {        
        Vector3 pos = MainCamera.transform.position;
        Skydome.transform.position = new Vector3(pos.x - this.skydomeRadius, 0.0f, pos.z - this.skydomeRadius);
        
        Vector3 center = getSkydomeCenter();
        Skydome.transform.Translate(pos.x - center.x, this.skydomeHeight / 2f, pos.z - center.z);

        Skylight.transform.position = getSkydomeCenter();
    }

    /// <summary>
    ///  Gibt das momentane Zentrum der Mesh des Skydomes zurück.
    /// </summary>
    Vector3 getSkydomeCenter()
    {
        Renderer renderer = Skydome.GetComponent<Renderer>();
        return renderer.bounds.center;
    }

}
